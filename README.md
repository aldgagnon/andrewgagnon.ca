## Overview

Temporary website for andrewgagnon.ca

## References

- https://cloud.google.com/storage/docs/hosting-static-website
- https://cloud.google.com/load-balancing/docs/https/setting-up-http-https-redirect#setting_up_the_http_load_balancer
- https://search.google.com/search-console/welcome
